from django.conf.urls import url
from .views import UserViewSet, authenticate_user

urlpatterns = [
    url('signup', UserViewSet.as_view(actions={'post': 'create'})),
    url('signin', authenticate_user),
]