import jwt
from django.contrib.auth import user_logged_in, authenticate
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework_jwt.serializers import jwt_payload_handler

from apps.users.models import User
from apps.users.serializers import UserSerializer, UserSigninRequestSerializer, \
    UserSigninResponseSerializer
from main import settings

DETAIL_403 = 'Can not authenticate with the given credentials or the account has been deactivated.'


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (AllowAny,)


@swagger_auto_schema(methods=['post'], request_body=UserSigninRequestSerializer,
                     responses={200: UserSigninResponseSerializer,
                                403: '{"detail": DETAIL_403}'},
                     operation_id='user_signin')
@api_view(['POST'])
@permission_classes([AllowAny, ])
def authenticate_user(request):
    serializer = UserSigninRequestSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)

    email = serializer.validated_data['email']
    password = serializer.validated_data['password']

    user = authenticate(username=email, password=password)
    if (user is None) or (not user.is_active):
        res = {'detail': DETAIL_403}
        return Response(res, status=status.HTTP_403_FORBIDDEN)

    payload = jwt_payload_handler(user)
    token = jwt.encode(payload, settings.SECRET_KEY)
    user_details = {}
    user_details['token'] = token
    user_logged_in.send(sender=user.__class__, request=request, user=user)

    return Response(user_details, status=status.HTTP_200_OK)
