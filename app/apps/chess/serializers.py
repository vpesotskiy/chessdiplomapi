from rest_framework import serializers

from apps.chess import models


class TaskListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Task
        fields = ('id', 'name')

class TaskDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Task
        fields = ('id', 'name', 'fen', 'pgn', 'difficulty')


class TaskRandomQuerySerializer(serializers.Serializer):
    difficulty = serializers.CharField(required=False, max_length=50)
