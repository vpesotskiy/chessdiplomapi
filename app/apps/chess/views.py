import random

from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, views, status, viewsets, pagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.chess import models, serializers
from apps.users.permissions import IsStaffOrReadOnly


class TaskListView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsStaffOrReadOnly)
    serializer_class = serializers.TaskListSerializer
    queryset = models.Task.objects.all()

    @swagger_auto_schema(request_body=serializers.TaskDetailSerializer,
                         responses={200: serializers.TaskDetailSerializer})
    def post(self, request, format=None):
        serializer = serializers.TaskDetailSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TaskPaginatedListView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsStaffOrReadOnly)
    serializer_class = serializers.TaskListSerializer
    pagination_class = pagination.LimitOffsetPagination
    queryset = models.Task.objects.all()


class TaskDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated, IsStaffOrReadOnly)
    queryset = models.Task.objects.all()
    serializer_class = serializers.TaskDetailSerializer


class TaskRandomDetailView(views.APIView):
    permission_classes = (IsAuthenticated, IsStaffOrReadOnly)

    @swagger_auto_schema(operation_id='tasks_randomly',
                         query_serializer=serializers.TaskRandomQuerySerializer,
                         responses={200: serializers.TaskDetailSerializer})
    def get(self, request):
        task_id_list_queryset = models.Task.objects

        difficulty = request.query_params.get('difficulty', None)
        if difficulty:
            task_id_list_queryset = task_id_list_queryset.filter(difficulty=difficulty)

        task_id_list = task_id_list_queryset.values_list('id', flat=True)
        if len(task_id_list) == 0:
            return Response({'detail': f'Not found any tasks with the difficulty: {difficulty}'},
                            status=status.HTTP_404_NOT_FOUND)

        random_task_id = random.choice(task_id_list)
        random_task = models.Task.objects.get(pk=random_task_id)
        serializer = serializers.TaskDetailSerializer(random_task)

        return Response(serializer.data)
