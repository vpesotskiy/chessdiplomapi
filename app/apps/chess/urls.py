from django.urls import path

from apps.chess import views

urlpatterns = [
    path('', views.TaskListView.as_view(actions={'get': 'list', 'post': 'post'})),
    path('paginated', views.TaskPaginatedListView.as_view(actions={'get': 'list'})),
    path('randomly', views.TaskRandomDetailView.as_view()),
    path('<int:pk>', views.TaskDetailView.as_view()),
]
