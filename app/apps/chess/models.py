from django.db import models

DIFFICULTY_CHOISES = (
    ('easy', 'easy'),
    ('normal', 'normal'),
    ('hard', 'hard'),
)

class Task(models.Model):
    name = models.CharField(max_length=254, db_index=True, unique=True)
    fen = models.CharField(max_length=254)
    pgn = models.TextField()
    difficulty = models.CharField(max_length=50, choices=DIFFICULTY_CHOISES, default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
