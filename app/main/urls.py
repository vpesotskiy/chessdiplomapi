from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from drf_yasg import openapi

from drf_yasg.views import get_schema_view
from rest_framework import permissions

schema_view = get_schema_view(
    openapi.Info(
        title="ChessDiplom API",
        default_version='v1',
        description="The backend part of the ChessDiplom project.",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('tasks/', include('apps.chess.urls')),
    path('user/', include('apps.users.urls')),
]

urlpatterns = [url(r'^api/v1/', include(urlpatterns))]
